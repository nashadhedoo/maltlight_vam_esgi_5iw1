const express = require('express');
const app = express();

require('./config')(app); console.log("conf");
require('./boot')(app); app.logger.info("app");
require('./helpers')(app);  app.logger.info("help");
require('./middlewares')(app);  app.logger.info("mid");
require('./actions')(app);  app.logger.info("action");
require('./routes')(app); app.logger.info("routes");

app.logger.info(`App listening on http://localhost:${app.config.port}`);
app.listen(app.config.port);
