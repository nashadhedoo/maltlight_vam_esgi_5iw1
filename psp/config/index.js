const path = require('path');
const validator = require('jsonschema').Validator;
const v = new validator();

module.exports = app => {
  const env = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev' ? '-dev' : '';
  const config = `./config${env}.json`;

  app.config = require(path.resolve(__dirname, config));
  schema = require(path.resolve(__dirname, `config-validator`));

  let validation = v.validate(app.config, schema);
  if(!validation.valid){
    console.log({
      error: `ConfigLoadingError`, 
      name: `ConfigFormatDoesNotMatchValidator`,
      message: validation.errors
    });
    process.exit(1)
  }

};
