module.exports = app => {

  app.helpers = {
    ensureOne,
    ensureOneArray,
    isEmpty,
    reject,
  };

  /**
   * 
   * @param {number, string} data
   */
  function ensureOne(data) {
    return (data) ? data : Promise.reject();
  }

  /**
   * 
   * @param {Array} data
   */
  function ensureOneArray(data) {
    if (data === undefined || data.length === 0) {
      return Promise.reject()
    }
    return data;
  }

  /**
   * 
   * @param {object} obj 
   */
  function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

  /**
   *
   * @param {number} code 
   * @param {string} type 
   * @param {string} fields 
   * @param {string} data 
   * @param {string} message 
   * @param {object} error
   */
  function reject(code, type, fields, data, message, error) {
    return Promise.reject({
      code: code,
      type: type,
      fields: fields,
      data: data,
      message: message,
      err: error
    });
  }

};
