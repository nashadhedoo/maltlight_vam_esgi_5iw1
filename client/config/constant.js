export const LS_TOKEN = "LS_TOKEN";
export const LS_USER = "LS_USER";

export const ROLE_FREELANCER = "ROLE_FREELANCER";
export const ROLE_ENTREPRISE = "ROLE_ENTREPRISE";
export const ROLE_ADMIN = "ROLE_ADMIN";

export const HOME_TYPE_APARTMENT = "Full remote";
export const HOME_TYPE_HOUSE = "Sur place";

export const ROLES = [ROLE_FREELANCER, ROLE_ENTREPRISE, ROLE_ADMIN];

export const HOME_STATUS = {
  CREATED: 'CREATED',
  VERIFIED: 'VERIFIED',
  REJECTED: 'REJECTED',
}

export const EVENT_STATUS = {
  CREATED: 'CREATED',
  VERIFIED: 'VERIFIED',
  REJECTED: 'REJECTED',
}
