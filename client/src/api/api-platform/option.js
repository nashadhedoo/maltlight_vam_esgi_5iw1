import {API_OPTIONS} from "../../../config/entrypoint";
import {request} from "@/utils";

export const getCompetences = async (params = []) => {
  return await request(API_OPTIONS, {
    withMetadata: true,
    query: params,
  });
};

export const getCompetence = async (id) => {
  return await request(`${API_OPTIONS}`, {id});
};

export const createCompetence = async (values) => {
  return await request(API_OPTIONS, {
    method: "POST",
    body: {
      name: values.name
    },
  });
};

export const editCompetence = async (values) => {
  return await request(API_OPTIONS, {
    method: "PUT",
    id: values.id,
    body: {
      name: values.name
    },
  });
};

export const deleteCompetence = async (id) => {
  return await request(`${API_OPTIONS}/${id}`, {
    method: "DELETE",
  });
};
