import EventsList from "@/components/Admin/Event/EventList";
import EventsEdit from "@/components/Admin/Event/EventEdit";
import EntrepriseLayout from "@/layout/Security/EntrepriseLayout";

const missionRoutes = [
  {
    path: "mission",
    component: EntrepriseLayout,
    children: [
      {
        path: "",
        component: EventsList,
        name: "back_mission",
      },
      {
        path: ":id/edit",
        component: EventsEdit,
        name: "back_mission_edit",
      },
    ],
  },

];

export default missionRoutes;
