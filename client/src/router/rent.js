import RentList from "@/components/Admin/Rent/RentList";
import EntrepriseLayout from "@/layout/Security/EntrepriseLayout";

const rentRoutes = [
  {
    path: 'rent',
    component: EntrepriseLayout,
    children: [
      {
        path: '',
        component: RentList,
        name: "back_rent"
      },
    ]
  },

];

export default rentRoutes;
