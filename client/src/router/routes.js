export const generateNameRoute = (base, route) => {
  return base + route.replace("/", "_");
};

export const RT_ROOT = "/";

export const RT_FRONT_MISSION_LIST = RT_ROOT + "mission-list"
export const RT_FRONT_HOMES = RT_ROOT + "homes";
export const RT_FRONT_HOMES_LOCATION = RT_FRONT_HOMES + "/location";
export const RT_FRONT_EVENTS = RT_ROOT + "missions"
export const RT_FRONT_NOUS = RT_ROOT + "nous"
export const RT_FRONT_CONTACT = RT_ROOT + "contact"
export const RT_FRONT_EVENTS_SHOW = RT_FRONT_EVENTS + "/:id"
