<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\User\ResetPasswordController;
use App\Controller\User\TokenResetPasswordController;
use App\Doctrine\Filter\FullTextFilter;

/**
 * @ApiResource(
 *     attributes={
 *          "security"="is_granted('ROLE_ADMIN') or object == user",
 *          "normalization_context"={"groups"={"user_read"}},
 *          "order"={"createdAt": "DESC"}
 *     },
 *    itemOperations={
 *      "get"={
 *        "security"="is_granted('ROLE_ADMIN') or object === user",
 *      },
 *      "put"={
 *         "security"="is_granted('ROLE_ADMIN') or object === user",
 *      },
 *      "delete"={
 *         "security"="is_granted('ROLE_ADMIN') or object === user",
 *      }
 *   },
 *     collectionOperations={
 *          "post"={
 *             "security"="true"
 *          },
 *          "get"={
 *             "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "reset-password"={
 *              "security"="true",
 *              "path"="/reset_password",
 *              "method"="POST",
 *              "controller"=ResetPasswordController::class,
 *          },
 *          "verify-token-password"={
 *              "security"="true",
 *              "path"="/token_reset_password",
 *              "method"="POST",
 *              "controller"=TokenResetPasswordController::class,
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity("email", message="user.unique")
 * @ApiFilter(SearchFilter::class, properties={"email": "partial"})
 * @ApiFilter(FullTextFilter::class, properties={"full_text"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class User implements UserInterface
{

  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   * @Groups({"user_read"})
   */
  private ?string $id;

  /**
   * @ORM\Column(type="string", length=180, unique=true)
   * @Assert\Email(message="user.errors.email")
   * @Assert\NotBlank(message="user.errors.required.email")
   * @Groups({"user_read", "user_write"})
   */
  private ?string $email;

  /**
   * @ORM\Column(type="json")
   * @Groups("user_read")
   */
  private array $roles = [];

  /**
   * @var string The hashed password
   * @ORM\Column(type="string")
   */
  private string $password;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank(message="user.errors.required.firstname")
   * @Groups({"user_read, user_write"})
   */
  private ?string $firstname;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank(message="user.errors.required.lastname")
   * @Groups({"user_read, user_write"})
   */
  private ?string $lastname;

  /**
   * @ORM\Column(type="string", length=15)
   * @Assert\NotBlank(message="user.errors.required.phone")
   * @Groups({"user_read, user_write"})
   */
  private ?string $phone;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $token;

  /**
   * @ORM\Column(type="date")
   * @Groups({"user_read", "user_write"})
   */
  private $createdAt;

  /**
   * @ORM\Column(type="date")
   * @Groups({"uuser_read, "user_write"})
   */
  private $updatedAt;

    /**
   * @ORM\Column(type="date")
   * @Groups({""user_read", "user_write"})
   */
  private $deletedAt;

  public function getId(): ?string
  {
    return $this->id;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;

    return $this;
  }

  /**
   * A visual identifier that represents this user.
   *
   * @see UserInterface
   */
  public function getUsername(): string
  {
    return (string)$this->email;
  }

  /**
   * @see UserInterface
   */
  public function getRoles(): array
  {
    $roles = $this->roles;
    // guarantee every user at least has ROLE_FREELANCER
    $roles[] = 'ROLE_FREELANCER';

    return array_unique($roles);
  }

  public function setRoles(array $roles): self
  {
    $this->roles = $roles;

    return $this;
  }

  /**
   * @see UserInterface
   */
  public function getPassword(): string
  {
    return (string)$this->password;
  }

  public function setPassword(string $password): self
  {
    $this->password = $password;

    return $this;
  }

  /**
   * @see UserInterface
   */
  public function getSalt()
  {
    // not needed when using the "bcrypt" algorithm in security.yaml
  }

  /**
   * @see UserInterface
   */
  public function eraseCredentials()
  {
    // If you store any temporary, sensitive data on the user, clear it here
    // $this->plainPassword = null;
  }

  public function getFirstname(): ?string
  {
    return $this->firstname;
  }

  public function setFirstname(string $firstname): self
  {
    $this->firstname = $firstname;

    return $this;
  }

  public function getLastname(): ?string
  {
    return $this->lastname;
  }

  public function setLastname(string $lastname): self
  {
    $this->lastname = $lastname;

    return $this;
  }

  /**
   * @Groups({"user_read", "mission_read"})
   * @return string|null
   */
  public function getFullName(): ?string
  {
    return strtoupper($this->lastname) . " " . $this->firstname;
  }

  public function getPhone(): ?string
  {
    return $this->phone;
  }

  public function setPhone(string $phone): self
  {
    $this->phone = $phone;

    return $this;
  }

  public function getToken(): ?string
  {
    return $this->token;
  }

  public function setToken(?string $token): self
  {
    $this->token = $token;

    return $this;
  }

  public function getCreatedAt(): ?\DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): self
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  public function getUpdatedAt(): ?\DateTimeInterface
  {
    return $this->updatedAt;
  }

  public function setUpdatedAt(\DateTimeInterface $updatedAt): self
  {
    $this->updatedAt = $updatedAt;

    return $this;
  }

  public function getdeletedAt(): ?\DateTimeInterface
  {
    return $this->deletedAt;
  }

  public function setDeletedAt(\DateTimeInterface $deletedAt): self
  {
    $this->deletedAt = $deletedAt;

    return $this;
  }
}
