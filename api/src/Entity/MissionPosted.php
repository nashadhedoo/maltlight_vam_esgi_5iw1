<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"createdAt": "DESC"},
 *      "normalization_context"={"groups"={"mission_read"}},
 *      "denormalization_context"={"groups"={"mission_write"}},
 *   },
 *   itemOperations={
 *      "get",
 *      "put"={
 *         "security"="is_granted('ROLE_ADMIN') == freelance",
 *      },
 *      "delete"={
 *         "security"="is_granted('ROLE_ADMIN')",
 *      },
 *      "status"={
 *        "security"="is_granted('ROLE_ENTREPRISE')",
 *        "path"="/missions/{id}/status",
 *        "method"="POST",
 *        "controller"=StatusController::class,
 *      }
 *
 *   },
 *   collectionOperations={
 *      "post",
 *      "get" = {
 *         "security"="true"
 *       },
 *   }
 * )
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 * @ApiFilter(DateFilter::class, properties={"startDate", "endDate"})
 * @ApiFilter(SearchFilter::class, properties={"mission.owner": "exact", "status": "exact"})
 * @UserAware(userFieldNames={"mission.owner", "mission.owner"})
 */
class MissionPosted
{
  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   * @Groups({"mission_posted_read""})
   */
  private ?string $id;

  /**
   * @ORM\Column(type="date")
   * @Groups({"mission_posted_read", "mission_posted_write"})
   * @Assert\GreaterThan("today")
   */
  private $startDate;

  /**
   * @ORM\Column(type="date")
   * @Groups({"mission_posted_read", "mission_posted_write"})
   * @Assert\GreaterThan(propertyPath="startDate")
   */
  private $endDate;

    /**
   * @ORM\Column(type="integer")
   * @Groups({"mission_posted_read", "mission_posted_write"})
   * @Assert\Positive
   */
  private $totalPayment;

    /**
   * @ORM\ManyToOne(targetEntity=Mission::class")
   * @Groups({"mission_read"})
   */
  private $mission;

      /**
   * @ORM\ManyToOne(targetEntity=User::class")
   * @Groups({"user_read"})
   */
  private $user;

  public function getId(): ?string
  {
    return $this->id;
  }

  public function getStartDate(): ?\DateTimeInterface
  {
    return $this->startDate;
  }

  public function setStartDate(\DateTimeInterface $startDate): self
  {
    $this->startDate = $startDate;

    return $this;
  }

  public function getEndDate(): ?\DateTimeInterface
  {
    return $this->endDate;
  }

  public function setEndDate(\DateTimeInterface $endDate): self
  {
    $this->endDate = $endDate;

    return $this;
  }

  public function getTotalPayment(): ?int
  {
    return $this->totalPayment;
  }

  public function setTotalPayment(int $totalPayment): self
  {
    $this->totalPayment = $totalPayment;

    return $this;
  }

  public function getMission(): ?Mission
  {
    return $this->mission;
  }

  public function setMission(?Mission $mission): self
  {
    $this->mission = $mission;

    return $this;
  }

  public function getUser(): ?User
  {
    return $this->user;
  }

  public function setUser(?User $user): self
  {
    $this->user = $user;

    return $this;
  }



}
