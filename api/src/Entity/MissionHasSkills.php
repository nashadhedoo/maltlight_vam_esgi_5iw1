<?php

Namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SkillsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"id": "DESC"}
 *   },
 * )
 * @ORM\Entity(repositoryClass=SkillsRepository::class)
 */
class MissionHasSkills
{

  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   */
  private ?string $id;

/**
   * @ORM\ManyToOne(targetEntity=Skills::class, inversedBy="MissionHasSkills")
   * @Groups({"skills_read", "mission_has_skills_read"})
   */
  private $skills;

  /**
   * @ORM\ManyToOne(targetEntity=Mission::class, inversedBy="MissionHasSkills")
   * @Groups({"mission_read", "mission_has_skills_read"})
   */
  private $mission;


  public function getId(): ?string
  {
    return $this->id;
  }

  public function getSkills(): ?Skills
  {
    return $this->skills;
  }

  public function setSkills(?Skills $skills): self
  {
    $this->skills = $skills;

    return $this;
  }

  public function getMission(): ?Mission
  {
    return $this->mission;
  }

  public function setMission(?Mission $mission): self
  {
    $this->mission = $mission;

    return $this;
  }

}
