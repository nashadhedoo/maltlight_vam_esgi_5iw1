<?php

Namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SkillsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"id": "DESC"}
 *   },
 * )
 * @ORM\Entity(repositoryClass=SkillsRepository::class)
 */
class Skills
{

  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   */
  private ?string $id;

  /**
   * @ORM\Column(type="string", length=100)
   * @Groups({"skills_read", "skills_write"})
   */
  private ?string $name;

  public function __construct()
  {
  }

  public function getId(): ?string
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }
}
