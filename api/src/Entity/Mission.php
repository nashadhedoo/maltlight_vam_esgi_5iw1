<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\UserAware;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"createdAt": "DESC"},
 *      "normalization_context"={"groups"={"mission_read"}},
 *      "denormalization_context"={"groups"={"mission_write"}},
 *   },
 *   itemOperations={
 *      "get",
 *      "put"={
 *         "security"="is_granted('ROLE_ADMIN') == freelance",
 *      },
 *      "delete"={
 *         "security"="is_granted('ROLE_ADMIN')",
 *      },
 *      "status"={
 *        "security"="is_granted('ROLE_ENTREPRISE')",
 *        "path"="/missions/{id}/status",
 *        "method"="POST",
 *        "controller"=StatusController::class,
 *      }
 *
 *   },
 *   collectionOperations={
 *      "post",
 *      "get" = {
 *         "security"="true"
 *       },
 *   }
 * )
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 * @ApiFilter(DateFilter::class, properties={"startDate", "endDate"})
 * @ApiFilter(SearchFilter::class, properties={"mission.owner": "exact", "status": "exact"})
 * @UserAware(userFieldNames={"mission.owner", "mission.owner"})
 */
class Mission
{

  const CREATED = 'CREATED';
  const APPROVED = 'APPROVED';
  const REJECTED = 'REJECTED';

  const STATUS = [self::CREATED, self::APPROVED, self::REJECTED];

  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private ?string $id;

  /**
   * @ORM\Column(type="string", length=100)
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   * @Assert\NotBlank
   */
  private ?string $name;

  /**
   * @ORM\Column(type="integer")
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   * @Assert\Positive
   */
  private ?int $peopleLimit;

  /**
   * @ORM\Column(type="text", nullable=true)
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   */
  private ?string $description;

  /**
   * @ORM\Column(type="date")
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   * @Assert\GreaterThan("today")
   */
  private $startDate;

  /**
   * @ORM\Column(type="date")
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   * @Assert\GreaterThan(propertyPath="startDate")
   */
  private $endDate;

  /**
   * @ORM\Column(type="integer")
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   * @Assert\Positive
   */
  private ?int $tjm;

    /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private $address;

    /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private $city;

      /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private $country;

  /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"mission_read", "mission_freelance_read"})
   * @Assert\Choice(choices=Mission::STATUS, message="mission.errors.status")
   */
  private $status;

      /**
   * @ORM\Column(type="string", length=255)
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private $type;

  /**
   * @ORM\ManyToOne(targetEntity=User::class")
   * @Groups({"user_read"})
  */
  private $user;

  public function getId(): ?string
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getPeopleLimit(): ?int
  {
    return $this->peopleLimit;
  }

  public function setPeopleLimit(int $peopleLimit): self
  {
    $this->peopleLimit = $peopleLimit;

    return $this;
  }

  public function getDescription(): ?string
  {
    return $this->description;
  }

  public function setDescription(?string $description): self
  {
    $this->description = $description;

    return $this;
  }

  public function getStartDate(): ?\DateTimeInterface
  {
    return $this->startDate;
  }

  public function setStartDate(\DateTimeInterface $startDate): self
  {
    $this->startDate = $startDate;

    return $this;
  }

  public function getEndDate(): ?\DateTimeInterface
  {
    return $this->endDate;
  }

  public function setEndDate(\DateTimeInterface $endDate): self
  {
    $this->endDate = $endDate;

    return $this;
  }

  public function getTjm(): ?int
  {
    return $this->tjm;
  }

  public function setTjm(int $tjm): self
  {
    $this->tjm = $tjm;

    return $this;
  }

  public function getAddress(): ?string
  {
    return $this->address;
  }

  public function setAddress(string $address): self
  {
    $this->address = $address;

    return $this;
  }

  public function getCity(): ?string
  {
    return $this->city;
  }

  public function setCity(string $city): self
  {
    $this->city = $city;

    return $this;
  }

  public function getCountry(): ?string
  {
    return $this->country;
  }

  public function setCountry(string $country): self
  {
    $this->country = $country;

    return $this;
  }

  public function getStatus(): ?string
  {
    return $this->status;
  }

  public function setStatus(string $status): self
  {
    $this->status = $status;

    return $this;
  }

  public function getType(): ?string
  {
    return $this->type;
  }

  public function setType(string $type): self
  {
    $this->type = $type;

    return $this;
  }

  public function getUser(): ?User
  {
    return $this->user;
  }

  public function setUser(?User $user): self
  {
    $this->user = $user;

    return $this;
  }
}
