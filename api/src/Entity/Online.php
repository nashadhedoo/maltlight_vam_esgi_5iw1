<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"createdAt": "DESC"},
 *      "normalization_context"={"groups"={"mission_read"}},
 *      "denormalization_context"={"groups"={"mission_write"}},
 *   },
 *   itemOperations={
 *      "get",
 *      "put"={
 *         "security"="is_granted('ROLE_ADMIN') == freelance",
 *      },
 *      "delete"={
 *         "security"="is_granted('ROLE_ADMIN')",
 *      },
 *      "status"={
 *        "security"="is_granted('ROLE_ENTREPRISE')",
 *        "path"="/missions/{id}/status",
 *        "method"="POST",
 *        "controller"=StatusController::class,
 *      }
 *
 *   },
 *   collectionOperations={
 *      "post",
 *      "get" = {
 *         "security"="true"
 *       },
 *   }
 * )
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 * @ApiFilter(DateFilter::class, properties={"startDate", "endDate"})
 * @ApiFilter(SearchFilter::class, properties={"mission.owner": "exact", "status": "exact"})
 * @UserAware(userFieldNames={"mission.owner", "mission.owner"})
 */
class Online
{
  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   * @Groups({"mission_read", "mission_freelance_read"})
   */
  private ?string $id;

  /**
   * @ORM\Column(type="string", length=100)
   * @Groups({"mission_read", "mission_write"})
   * @Assert\NotBlank
   */
  private ?string $name;

  /**
   * @ORM\Column(type="integer")
   * @Groups({"mission_read", "mission_write"})
   * @Assert\Positive
   */
  private ?int $peopleLimit;

  /**
   * @ORM\Column(type="text", nullable=true)
   * @Groups({"mission_read", "mission_write", "mission_freelance_read"})
   */
  private ?string $description;

    /**
   * @ORM\ManyToOne(targetEntity=Mission::class, inversedBy="Online")
   * @Groups({"Online_read", "Online_write"})
   */
  private $missionPosted;

  public function __construct()
  {
  }

  public function getId(): ?string
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getPeopleLimit(): ?int
  {
    return $this->peopleLimit;
  }

  public function setPeopleLimit(int $peopleLimit): self
  {
    $this->peopleLimit = $peopleLimit;

    return $this;
  }

  public function getDescription(): ?string
  {
    return $this->description;
  }

  public function setDescription(?string $description): self
  {
    $this->description = $description;

    return $this;
  }

  public function getMissionPosted(): ?MissioPosted
  {
    return $this->missionPosted;
  }

  public function setMissionPosted(?MissionPosted $missionPosted): self
  {
    $this->missionPosted = $missionPosted;

    return $this;
  }

}
