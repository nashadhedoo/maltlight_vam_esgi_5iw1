<?php

Namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserHasOnlineRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *   attributes={
 *      "order"={"id": "DESC"}
 *   },
 * )
 * @ORM\Entity(repositoryClass=UserHasOnlineRepository::class)
 */
class UserHasOnline
{

  /**
   * @ORM\Id
   * @ORM\Column(type="string", unique=true)
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="App\Doctrine\IdGenerator")
   */
  private ?string $id;

/**
   * @ORM\ManyToOne(targetEntity=Online::class, inversedBy="UserHasOnline")
   * @Groups({"online_read", "user_has_onlines_read"})
   */
  private $online;

  /**
   * @ORM\ManyToOne(targetEntity=User::class, inversedBy="User")
   * @Groups({"user_read", "user_has_online_read"})
   */
  private $user;


  public function getId(): ?string
  {
    return $this->id;
  }

  public function getOnline(): ?Online
  {
    return $this->online;
  }

  public function setOnline(?Online $online): self
  {
    $this->online = $online;

    return $this;
  }

  public function getUser(): ?User
  {
    return $this->user;
  }

  public function setUser(?User $user): self
  {
    $this->user = $user;

    return $this;
  }

}
