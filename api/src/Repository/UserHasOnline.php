<?php

namespace App\Repository;

use App\Entity\UserHasOnline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserHasOnline|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserHasOnline|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserHasOnline[]    findAll()
 * @method UserHasOnline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserHasOnline::class);
    }

    // /**
    //  * @return UserHasOnline[] Returns an array of UserHasOnline objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserHasOnline
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
