<?php

namespace App\Repository;

use App\Entity\MissionHasOnline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MissionHasOnline|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissionHasOnline|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissionHasOnline[]    findAll()
 * @method MissionHasOnline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionHasOnlineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissionHasOnline::class);
    }

    // /**
    //  * @return MissionHasOnline[] Returns an array of MissionHasOnline objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MissionHasOnline
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
