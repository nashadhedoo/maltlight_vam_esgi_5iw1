<?php

namespace App\Repository;

use App\Entity\MissionPosted;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MissionPosted|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissionPosted|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissionPosted[]    findAll()
 * @method MissionPosted[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionPosted extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissionPosted::class);
    }

    public function getPopularMissions()
    {
      return $this->createQueryBuilder('mission')
        ->where('mission.startDate < :now')
        ->andWhere('mission.endDate > :now')
        ->setParameters([
          'now' => date("Y-m-d")
        ])
        ->getQuery()
        ->getResult();
    }



    /*
    public function findOneBySomeField($value): ?MissionPosted
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
