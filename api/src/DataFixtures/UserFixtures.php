<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

  private UserPasswordEncoderInterface $passwordEncoder;

  public function __construct(UserPasswordEncoderInterface $passwordEncoder)
  {
    $this->passwordEncoder = $passwordEncoder;
  }

  public function load(ObjectManager $manager)
  {
    $admin = (new User())
      ->setEmail("nash1@gmail.com")
      ->setFirstname("nashad")
      ->setLastname("hedoo")
      ->setPhone("0123456KT789")
      ->setRoles(["ROLE_ADMIN"]);
    $admin->setPassword($this->passwordEncoder->encodePassword($admin, "nashnash"));
    $manager->persist($admin);

    $renter = (new User())
      ->setEmail("nash2@gmail.com")
      ->setFirstname("Prenom")
      ->setLastname("Nom")
      ->setPhone("0123456789")
      ->setRoles(["ROLE_FREELANCER"]);
    $renter->setPassword($this->passwordEncoder->encodePassword($renter, "freelancer"));
    $manager->persist($renter);

    $renter2 = (new User())
      ->setEmail("nash3@gmail.com")
      ->setFirstname("Prenom")
      ->setLastname("Nom")
      ->setPhone("0123456789")
      ->setRoles(["ROLE_ENTREPRISE"]);
    $renter2->setPassword($this->passwordEncoder->encodePassword($renter2, "entreprise"));
    $manager->persist($renter2);



    $manager->flush();


  }

}
