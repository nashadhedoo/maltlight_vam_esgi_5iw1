<?php

namespace App\DataFixtures;

use App\Entity\Skills;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SkillsFixtures extends Fixture
{

  public function load(ObjectManager $manager)
  {
    $skills = [];

    $skills[] = (new Skills())
      ->setName("PHP");

    $skills[] = (new Skills())
      ->setName("ReactJS");

    $skills[] = (new Skills())
      ->setName("React native");

    $skills[] = (new Skills())
      ->setName("Vue js");

    $skills[] = (new Skills())
      ->setName("Api Platform");

    $skills[] = (new Skills())
      ->setName("SASS");

    $skills[] = (new Skills())
      ->setName("C++");

    $skills[] = (new Skills())
      ->setName("C#");

    $skills[] = (new Skills())
      ->setName("C");

    foreach ($skills as $skills) {
      $manager->persist($skills);
    }

    $manager->flush();
  }
}
