<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201118222005 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('CREATE SEQUENCE mission_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE SEQUENCE mission_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE SEQUENCE competence_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE SEQUENCE picture_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE SEQUENCE service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
    $this->addSql('CREATE TABLE mission_user (id VARCHAR(255) NOT NULL, owner_id VARCHAR(255) DEFAULT NULL, mission_id VARCHAR(255) DEFAULT NULL, is_owner BOOLEAN NOT NULL, PRIMARY KEY(id))');
    $this->addSql('CREATE INDEX IDX_92589AE27E3C61F9 ON mission_user (owner_id)');
    $this->addSql('CREATE INDEX IDX_92589AE271F7E88B ON mission_user (mission_id)');
    $this->addSql('CREATE TABLE competence (id VARCHAR(255) NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
    $this->addSql('CREATE TABLE service (id VARCHAR(255) NOT NULL, name VARCHAR(100) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
    $this->addSql('CREATE TABLE "user" (id VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(100) NOT NULL, lastname VARCHAR(100) NOT NULL, phone VARCHAR(15) NOT NULL, company VARCHAR(255) DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
    $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
    $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_92589AE27E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_92589AE271F7E88B FOREIGN KEY (mission_id) REFERENCES mission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    $this->addSql('CREATE EXTENSION IF NOT EXISTS unaccent');
    $this->addSql('DROP TEXT SEARCH CONFIGURATION IF EXISTS mydict');
    $this->addSql('CREATE TEXT SEARCH CONFIGURATION mydict ( COPY = simple )');
    $this->addSql('ALTER TEXT SEARCH CONFIGURATION mydict ALTER MAPPING FOR hword, hword_part, word WITH unaccent, simple');

    $admin = '$argon2id$v=19$m=65536,t=4,p=1$CX+1D6E9oCTGz6GId18+sQ$KpGqGrzznHuOMEUwSakpalPg+lTdljBYMrDc3naGFOc';

    $this->addSql("INSERT INTO public.user(id, email, password, lastname, firstname, roles, phone, created_at, updated_at)
                VALUES (nextval('user_id_seq'),
                        'admin@avg.com',
                        '".$admin."',
                        'Admin lastname',
                        'Admin firstname',
                        '[\"ROLE_ADMIN\"]',
                        '0123456789',
                        '2020-11-06 09:01:22',
                        '2020-11-06 09:01:22')");
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE mission_user DROP CONSTRAINT FK_92589AE271F7E88B');
    $this->addSql('ALTER TABLE competence DROP CONSTRAINT FK_99D8A63328CDC89C');
    $this->addSql('ALTER TABLE picture DROP CONSTRAINT FK_16DB4F8928CDC89C');
    $this->addSql('ALTER TABLE competence DROP CONSTRAINT FK_99D8A633A7C41D6F');
    $this->addSql('ALTER TABLE mission DROP CONSTRAINT FK_3BAE0AA7E5FD6250');
    $this->addSql('DROP SEQUENCE mission_id_seq CASCADE');
    $this->addSql('DROP SEQUENCE mission_user_id_seq CASCADE');
    $this->addSql('DROP SEQUENCE competence_id_seq CASCADE');
    $this->addSql('DROP SEQUENCE picture_id_seq CASCADE');
    $this->addSql('DROP SEQUENCE service_id_seq CASCADE');
    $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
    $this->addSql('DROP TABLE mission');
    $this->addSql('DROP TABLE mission_user');
    $this->addSql('DROP TABLE competence');
    $this->addSql('DROP TABLE picture');
    $this->addSql('DROP TABLE service');
    $this->addSql('DROP TABLE "user"');

    $this->addSql('DROP TEXT SEARCH CONFIGURATION mydict');
    $this->addSql('DROP EXTENSION unaccent');
  }
}
